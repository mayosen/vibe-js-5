"use strict";

const personalMovieDB = {
  count: 0,
  movies: {},
  actors: {},
  genres: [],
  isPrivate: false,
  database: document.querySelector(".database"),

  toggleVisibleMyDB: function() {
    this.database.replaceChildren();
    this.isPrivate = !this.isPrivate;
  },

  showMyDB: function() {
    if (!personalMovieDB.isPrivate) {
      let count = document.createElement("div");
      count.innerText = `count: ${this.count}`;
      let movies = document.createElement("div");
      movies.innerText = `movies: ${JSON.stringify(this.movies)}`;
      let actors = document.createElement("div");
      actors.innerText = `actors: ${JSON.stringify(this.actors)}`;
      let genres = document.createElement("div");
      genres.innerText = `genres: ${JSON.stringify(this.genres)}`;

      this.database.replaceChildren();
      [count, movies, actors, genres].forEach(item => this.database.append(item));
    }
  },

  writeYourMovies: function() {
    this.count = +prompt("Сколько фильмов вы уже посмотрели?");
    let answersGiven = 0;

    while (answersGiven < this.count) {
      let lastViewedMovie = prompt("Один из последних просмотренных фильмов?");
      if (lastViewedMovie === null || lastViewedMovie === "" || lastViewedMovie.length > 50) {
        alert("Повторите ввод");
        continue;
      }
      this.movies[lastViewedMovie] = +prompt("На сколько оцените его?");
      answersGiven++;
    }

    if (this.count < 10) {
      alert("Просмотрено довольно мало фильмов");
    } else if (10 <= this.count && this.count <= 30) {
      alert("Вы классический зритель");
    } else if (this.count > 30) {
      alert("Вы киноман");
    } else {
      alert("Произошла ошибка");
    }
  },

  writeYourGenres: function () {
    let genresGiven = 0;
    while (genresGiven < 3) {
      let genre = prompt(`Ваш любимый жанр под номером ${genresGiven + 1}`);
      if (genre === null || genre === "") {
        alert("Повторите ввод");
        continue;
      }
      this.genres.push(genre);
      genresGiven++;
    }

    this.genres.forEach((item, i) => {
      console.log(`Любимый жанр #${i + 1} - это ${item}`);
    });
  }
};

document.querySelector("body").style.cssText = `
  font-family: Arial, sans-serif;
  padding: 100px 200px;
`;

personalMovieDB.database.insertAdjacentHTML("afterend", `<div class="banner">Banner</div>`);
document.querySelector(".banner").innerHTML = `<a target="_blank" href="#">View banner</a>`;
document.querySelectorAll("button").forEach(item => {
  let style = item.style;
  style.margin = "10px";
  style.padding = "10px";
  style.border = "1px solid black";
  style.borderRadius = "5px";
});

let banner = document.querySelector(".banner");
banner.classList.add("border");
